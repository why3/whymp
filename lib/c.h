#ifndef C_H_INCLUDED

#include "uint32gmp.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <alloca.h>
#define IGNORE2(x,y) do { (void)(x); (void)(y); } while (0)
#define IGNORE3(x,y,z) do { (void)(x); (void)(y); (void)(z); } while (0)

#define C_H_INCLUDED
#endif // C_H_INCLUDED
