#ifndef COMPARE_H_INCLUDED

#include "uint64gmp.h"
#include "c.h"
#include <stdint.h>

int32_t wmpn_cmp(uint64_t * x, uint64_t * y, int32_t sz);

#define COMPARE_H_INCLUDED
#endif // COMPARE_H_INCLUDED
