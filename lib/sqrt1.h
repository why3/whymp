#ifndef SQRT1_H_INCLUDED

#include "c.h"
#include "fxp.h"

uint64_t sqrt1(uint64_t * rp, uint64_t a0);

#define SQRT1_H_INCLUDED
#endif // SQRT1_H_INCLUDED
