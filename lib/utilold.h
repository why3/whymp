#ifndef UTILOLD_H_INCLUDED

#include "uint64gmp.h"
#include "c.h"
#include <stdint.h>

void wmpn_copyi1(uint64_t * r, uint64_t * x, int32_t sz);

#define UTILOLD_H_INCLUDED
#endif // UTILOLD_H_INCLUDED
